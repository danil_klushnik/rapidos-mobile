import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
    paddingTop: 40,
    paddingBottom: 20,
    justifyContent: 'space-between'
  },

  titleContainer: {
    alignItems: 'center'
  },

  title: {
    fontSize: 25
  }
});

export default styles;
