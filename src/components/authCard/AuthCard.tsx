import React from 'react';
import {Text, View} from 'react-native';
import styles from './AuthCard.styles';
import {Button} from '../index';

interface AuthCardProps {
  buttonTitle: string;
  buttonOnPress: () => void;
  bottomText?: Element;
}

const AuthCard: React.FC<AuthCardProps> = ({
  children,
  buttonOnPress,
  buttonTitle,
  bottomText,
}) => {
  return (
    <View style={styles.container}>
      <View style={styles.titleContainer}>
        <Text style={styles.title}>{'Rapidos.sh'}</Text>
      </View>
      <View>
        {children}
        {bottomText && (
          <View style={{alignItems: 'flex-end'}}>{bottomText}</View>
        )}
      </View>
      <Button
        title={buttonTitle}
        onPress={buttonOnPress}
        visualStyle={'solid'}
      />
    </View>
  );
};

export default AuthCard;
