import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    borderBottomWidth: 1,
    borderBottomColor: '#000',
    paddingLeft: 3,
  },
  text: {
    fontSize: 12,
    color: '#000',
  },
});

export default styles;
