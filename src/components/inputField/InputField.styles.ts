import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    paddingBottom: '4%',
    width: '100%',
  },

  textInput: {
    paddingBottom: 5,
    paddingLeft: 5,
    paddingTop: 5,
    borderWidth: 1,
    color: '#000000',
  },

  textInputFull: {
    borderColor: '#000000',
  },

  textInputEmpty: {
    borderColor: '#000000',
  },
});

export default styles;
