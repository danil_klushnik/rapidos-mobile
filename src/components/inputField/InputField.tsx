import React from 'react';
import {TextInput, View, ViewStyle} from 'react-native';
import styles from './InputField.styles';

interface InputFieldProps {
  placeholder?: string;
  placeholderTextColor?: string;
  onChangeText?: (text: string) => void;
  value?: string;
  type?: 'none' | 'emailAddress' | 'password' | 'telephoneNumber';
  style?: ViewStyle;
}

const InputField: React.FC<InputFieldProps> = ({
  style,
  placeholder,
  placeholderTextColor,
  children,
  onChangeText,
  value,
  type,
  ...otherProps
}) => {
  let autoCapitalize: 'none' | 'sentences' | 'words' | 'characters' | undefined;

  let inputBorderStyle: ViewStyle;

  if (value == null || value === '') {
    inputBorderStyle = styles.textInputEmpty;
  } else {
    inputBorderStyle = styles.textInputFull;
  }

  return (
    <View style={[styles.container, style]}>
      <TextInput
        underlineColorAndroid="transparent"
        placeholderTextColor="#D6D6D6"
        style={{
          ...styles.textInput,
          ...inputBorderStyle,
        }}
        autoCapitalize={autoCapitalize}
        selectionColor="#0433BF"
        onChangeText={onChangeText}
        value={value}
        placeholder={placeholder}
        {...otherProps}
        secureTextEntry={type === 'password'}
        textContentType={!type ? 'none' : type}
      />
    </View>
  );
};

export default InputField;
