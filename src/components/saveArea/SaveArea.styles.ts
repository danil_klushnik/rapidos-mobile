import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  menuContainer: {
    alignItems: 'flex-end',
    paddingTop: 20,
    paddingRight: 20,
  },
});

export default styles;
