import React from 'react';
import {Image, TouchableOpacity, View} from 'react-native';
import {Menu} from './assets';
import styles from './SaveArea.styles';

interface SaveAreaProps {}

const SaveArea: React.FC<SaveAreaProps> = ({children}) => {
  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.menuContainer}>
        <Image source={Menu} />
      </TouchableOpacity>
      <View style={{flex: 1}}>{children}</View>
    </View>
  );
};

export default SaveArea;
