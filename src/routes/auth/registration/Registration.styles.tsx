import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
    bottomTextContainer: {
        flexDirection: 'row',
    },

    textLink: {
        fontSize: 15,
    },
});

export default styles;
