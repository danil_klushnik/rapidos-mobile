import React, {useState} from 'react';
import {ScrollView, Text, View} from 'react-native';
import {AuthCard, InputField, TextLink} from '../../../components';
import styles from './Registration.styles';
import {useTranslation} from 'react-i18next';
import {useRouterActions} from "../../../state/hooks/UseActions";

const Registration: React.FC = () => {
  const {t} = useTranslation('registration');
  const routerActions = useRouterActions();

  const [name, setName] = useState<string>('');
  const [lastName, setLastName] = useState<string>('');
  const [phoneNumber, setPhoneNumber] = useState<string>('');
  const [confirmPass, setConfirmPass] = useState<string>('');
  const [email, setEmail] = useState<string>('');
  const [password, setPassword] = useState<string>('');

  const renderBottomText = () => (
    <View style={styles.bottomTextContainer}>
      <Text>{t('avoir un compte?')}</Text>
      <TextLink
        onPress={() => undefined}
        text={t("s'identifier")}
        styleText={styles.textLink}
        link="/auth/logIn"
      />
    </View>
  );

  return (
    <AuthCard
      buttonOnPress={() => routerActions.navigateToMain()}
      buttonTitle={t('Confirmez')}
      bottomText={renderBottomText()}>
      <ScrollView>
        <InputField
          onChangeText={setName}
          placeholder={t('Nom')}
          value={name}
        />
        <InputField
          onChangeText={setLastName}
          placeholder={t('Prénom')}
          value={lastName}
        />
        <InputField
          onChangeText={setEmail}
          placeholder={t('Email')}
          type={'emailAddress'}
          value={email}
        />
        <InputField
          onChangeText={setPhoneNumber}
          placeholder={t('Numéro de téléphone')}
          type={'telephoneNumber'}
          value={phoneNumber}
        />
        <InputField
          onChangeText={setPassword}
          placeholder={t('Mot de passe')}
          type={'password'}
          value={password}
        />
        <InputField
          onChangeText={setConfirmPass}
          placeholder={t('Confirmez le mot de passe')}
          type={'password'}
          value={confirmPass}
        />
      </ScrollView>
    </AuthCard>
  );
};

export default Registration;
