import Registration from './registration/Registration';
import LogIn from './login/LogIn';
import ForgotPassword from './forgotPassword/ForgotPassword';

export {Registration, LogIn, ForgotPassword};
