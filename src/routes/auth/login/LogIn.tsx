import React, {useState} from 'react';
import {ScrollView, Text, View} from 'react-native';
import {AuthCard, InputField, TextLink} from 'components';
import styles from '../registration/Registration.styles';
import {useTranslation} from 'react-i18next';
import {useRouterActions} from "../../../state/hooks/UseActions";

const LogIn: React.FC = () => {
  const {t} = useTranslation('logIn');
    const routerActions = useRouterActions()

  const [email, setEmail] = useState<string>('');
  const [password, setPassword] = useState<string>('');

  const renderBottomText = () => (
    <View style={{alignItems: 'flex-end'}}>
      <View style={styles.bottomTextContainer}>
        <Text>{t("Vous n'avez pas de compte?")}</Text>
        <TextLink
          onPress={() => undefined}
          text={t("s'inscrire")}
          styleText={styles.textLink}
          link="/auth/registration"
        />
      </View>
      <View style={styles.bottomTextContainer}>
        <Text>{t('Mot de passe oublié?')}</Text>
        <TextLink
          onPress={() => undefined}
          text={t('restaurer le mot de passe')}
          styleText={styles.textLink}
          link="/auth/forgotPass"
        />
      </View>
    </View>
  );

  return (
    <AuthCard
        buttonOnPress={() => routerActions.navigateToMain()}
      buttonTitle={t('Confirmez')}
      bottomText={renderBottomText()}>
      <ScrollView>
        <InputField
          onChangeText={setEmail}
          placeholder={t('Email')}
          type={'emailAddress'}
          value={email}
        />
        <InputField
          onChangeText={setPassword}
          placeholder={t('Mot de passe')}
          type={'password'}
          value={password}
        />
      </ScrollView>
    </AuthCard>
  );
};

export default LogIn;
