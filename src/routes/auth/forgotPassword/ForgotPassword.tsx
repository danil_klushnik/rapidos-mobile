import React, {useState} from 'react';
import {ScrollView, Text, View} from 'react-native';
import {useTranslation} from 'react-i18next';
import styles from '../registration/Registration.styles';
import {AuthCard, InputField, TextLink} from 'components';
import {useRouterActions} from '../../../state/hooks/UseActions';

const ForgotPassword: React.FC = () => {
  const {t} = useTranslation('forgotPassword');
  const routerActions = useRouterActions();

  const [email, setEmail] = useState<string>('');

  const renderBottomText = () => (
    <View style={styles.bottomTextContainer}>
      <Text>{t('avoir un compte?')}</Text>
      <TextLink
        onPress={() => undefined}
        text={t("s'identifier")}
        styleText={styles.textLink}
        link="/auth/logIn"
      />
    </View>
  );

  return (
    <AuthCard
      buttonOnPress={() => routerActions.navigateToMain()}
      buttonTitle={t('Confirmez')}
      bottomText={renderBottomText()}>
      <ScrollView>
        <InputField
          onChangeText={setEmail}
          placeholder={t('Email')}
          type={'emailAddress'}
          value={email}
        />
      </ScrollView>
    </AuthCard>
  );
};

export default ForgotPassword;
