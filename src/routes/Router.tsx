import React from 'react';
import {
  NativeRouter,
  Redirect,
  Route,
  Switch,
  MemoryRouter,
} from 'react-router-native';
import {View} from 'react-native';
import styles from './Router.styles';
import {ForgotPassword, LogIn, Registration} from './auth';
import {
  Menu,
  Appartements,
  Boutiques,
  Livereus,
  Restaurants,
  Supermarches,
  Taxis,
} from './main';

const Router: React.FC = () => {
  return (
    <MemoryRouter>
      <NativeRouter>
        <View style={styles.container}>
          <Switch>
            <Route exact path="/">
              <Redirect to="/main" />
            </Route>
            <Route exact path="/auth">
              <LogIn />
            </Route>
            <Route path="/main">
              <Route exact path="/main">
                <Menu />
              </Route>

              <Route exact path="/main/restaurants">
                <Restaurants />
              </Route>

              <Route exact path="/main/livereus">
                <Livereus />
              </Route>

              <Route exact path="/main/boutiques">
                <Boutiques />
              </Route>

              <Route exact path="/main/appartements">
                <Appartements />
              </Route>

              <Route exact path="/main/taxis">
                <Taxis />
              </Route>

              <Route exact path="/main/supermarches">
                <Supermarches />
              </Route>
            </Route>
            <Route path="/auth">
              <Route exact path="/auth/login">
                <LogIn />
              </Route>
              <Route exact path="/auth/registration">
                <Registration />
              </Route>
              <Route exact path="/auth/forgotPass">
                <ForgotPassword />
              </Route>
            </Route>
          </Switch>
        </View>
      </NativeRouter>
    </MemoryRouter>
  );
};

export default Router;
