import React from 'react';
import {Text} from 'react-native';
import {SaveArea} from 'components';

const Appartements: React.FC = () => {
  return (
    <SaveArea>
      <Text>Appartements</Text>
    </SaveArea>
  );
};

export default Appartements;
