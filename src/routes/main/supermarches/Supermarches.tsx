import React from 'react';
import {Text} from 'react-native';
import {SaveArea} from 'components';

const Supermarches: React.FC = () => {
  return (
    <SaveArea>
      <Text>Supermarches</Text>
    </SaveArea>
  );
};

export default Supermarches;
