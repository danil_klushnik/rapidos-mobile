import React from 'react';
import {Text} from 'react-native';
import {SaveArea} from 'components';

const Boutiques: React.FC = () => {
    return (
        <SaveArea>
            <Text>Boutiques</Text>
        </SaveArea>
    );
};

export default Boutiques;
