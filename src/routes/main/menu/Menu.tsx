import React from 'react';
import {ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {Button, SaveArea} from 'components';
import {useTranslation} from 'react-i18next';
import {useRouterActions} from '../../../state/hooks/UseActions';

const Menu: React.FC = () => {
  const {t} = useTranslation('menu');
  const routerActions = useRouterActions();

  const renderItem = (onPress: () => void, title: string) => (
    <TouchableOpacity
      onPress={onPress}
      style={{
        margin: 5,
        paddingVertical: 60,
        borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
      }}>
      <View>
        <Text>{title}</Text>
      </View>
    </TouchableOpacity>
  );

  const renderTwoElements = (
    firstItemTitle: string,
    firstItemOnPress: () => void,
    secondItemTitle: string,
    secondItemOnPress: () => void,
  ) => (
    <View style={{flexDirection: 'row', flex: 1}}>
      {renderItem(firstItemOnPress, firstItemTitle)}
      {renderItem(secondItemOnPress, secondItemTitle)}
    </View>
  );

  return (
    <SaveArea>
      <View style={{flex: 1}}>
        <ScrollView style={{height: '100%', padding: 20}}>
          {renderTwoElements(
            t('Taxis'),
            routerActions.navigateToTaxis,
            t('Livreur'),
            routerActions.navigateToLivereus,
          )}
          {renderTwoElements(
            t('Supermarchés'),
            routerActions.navigateToSupermarches,
            t('Restaurants'),
            routerActions.navigateToRestaurants,
          )}
          {renderTwoElements(
            t('Boutiques'),
            routerActions.navigateToBoutiques,
            t('Appartements'),
            routerActions.navigateToAppartements,
          )}
        </ScrollView>
      </View>
      <View style={{paddingBottom: 20, paddingHorizontal: 40}}>
        <Button
          onPress={() => routerActions.navigateToAuth()}
          title={t("Se Connecter / S'enregistrer")}
          visualStyle={'solid'}
        />
      </View>
    </SaveArea>
  );
};

export default Menu;
