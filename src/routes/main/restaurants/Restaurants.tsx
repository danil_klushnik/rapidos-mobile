import React from 'react';
import {Text} from 'react-native';
import {SaveArea} from 'components';

const Restaurants: React.FC = () => {
    return (
        <SaveArea>
            <Text>Restaurants</Text>
        </SaveArea>
    );
};

export default Restaurants;
