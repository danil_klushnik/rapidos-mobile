import Menu from './menu/Menu';
import Appartements from './appartements/Appartements';
import Taxis from './taxis/Taxis';
import Supermarches from './supermarches/Supermarches';
import Restaurants from './restaurants/Restaurants';
import Livereus from './livereus/Livereus';
import Boutiques from './boutiques/Boutiques';

export {
  Menu,
  Appartements,
  Taxis,
  Supermarches,
  Restaurants,
  Livereus,
  Boutiques,
};
