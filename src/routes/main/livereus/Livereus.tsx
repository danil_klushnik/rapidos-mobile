import React from 'react';
import {Text} from 'react-native';
import {SaveArea} from 'components';

const Livereus: React.FC = () => {
    return (
        <SaveArea>
            <Text>Livereus</Text>
        </SaveArea>
    );
};

export default Livereus;
