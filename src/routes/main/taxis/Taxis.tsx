import React from 'react';
import {Text} from 'react-native';
import {SaveArea} from 'components';

const Taxis: React.FC = () => {
    return (
        <SaveArea>
            <Text>Taxis</Text>
        </SaveArea>
    );
};

export default Taxis;
