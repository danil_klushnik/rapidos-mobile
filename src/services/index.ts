import IAlertService from './IAlertService';
import AlertService from './AlertService';
import IConfigService from './config/IConfigService';
import ConfigService from 'services/config/ConfigService';
import {getAppEnv, getNodeEnv} from 'services/config/ConfigUtils';

const alertService: IAlertService = new AlertService();

const configService: IConfigService = new ConfigService(getNodeEnv(), getAppEnv());

export {
  //
  alertService as AlertService,
  configService as ConfigService,
};
