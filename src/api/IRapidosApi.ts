import Session from 'auth/Session';
import RegisterRequest from './entities/RegisterRequest';
import LoginRequest from './entities/LoginRequest';
import ForgotPasswordRequest from 'api/entities/ForgotPasswordRequest';

export interface IRapidosApi {
  register(request: RegisterRequest): Promise<Session>;

  login(request: LoginRequest): Promise<Session>;

  forgotPassword(request: ForgotPasswordRequest): Promise<void>;
}
