import {RapidosApiTokenHolders} from 'api';
import Config from 'app/Config';
import ApiError from 'api/rest/core/ApiError';
import ApiHttpError from 'api/rest/core/ApiHttpError';
import AppType from "../entities/AppType";

export const getHeaders = () => {
  const token = RapidosApiTokenHolders.getToken();
  const headers: any = {
    app: AppType.Client
        // Config.getAppType()
    ,
    platform: Config.getPlatform(),
  };
  if (token) {
    headers.authorization = token;
  }
  return headers;
};

export const checkNotAuthorizedError = (e: ApiHttpError): boolean => {
  if (e instanceof ApiError) {
    return e.status === 401;
  }
  return false;
};

