import {createConfiguration} from './ApiConfiguration';
import RapidosApi from './RapidosApi';
import {IRapidosApi} from './IRapidosApi';
import IRapidosApiTokenHolders from 'api/IRapidosApiTokenHolders';
import RapidosApiTokenHolders from './RapidosApiTokenHolders';

const config = createConfiguration();

const rapidosApi: IRapidosApi = new RapidosApi(config);

const rapidosApiTokenHolders: IRapidosApiTokenHolders = new RapidosApiTokenHolders();

export {
  rapidosApi as RapidosApi,
  rapidosApiTokenHolders as RapidosApiTokenHolders,
};
