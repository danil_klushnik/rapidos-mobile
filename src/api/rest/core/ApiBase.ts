import axios, {
  AxiosError,
  AxiosInstance,
  AxiosPromise,
  AxiosRequestConfig,
  AxiosResponse,
} from 'axios';
import * as AxiosLogger from 'axios-logger';
import ApiError from './ApiError';
import ApiHttpError from './ApiHttpError';
import RestApiConfiguration from 'api/rest/core/RestApiConfiguration';
import ApiDelegate from 'api/core/ApiDelegate';

export default class ApiBase {
  private static readonly HEADERS = {};

  private readonly api: AxiosInstance;

  public url: string | undefined;

  protected static wrapApiCall(call: AxiosPromise): Promise<any> {
    return call
      .then((response) => Promise.resolve(ApiBase.handleResponse(response)))
      .catch((response) => Promise.reject(ApiBase.handleError(response)));
  }

  constructor(
    baseUrl: string,
    configuration: RestApiConfiguration,
    private delegate: ApiDelegate,
  ) {
    this.url = `${baseUrl}${configuration.path}`;
    this.api = axios.create({
      baseURL: this.url,
      headers: ApiBase.HEADERS,
    });
    this.api.interceptors.request.use((value) => {
      return {...value, headers: {...value.headers, ...this.delegate.getHeaders()}};
    });
    this.api.interceptors.request.use(AxiosLogger.requestLogger, AxiosLogger.errorLogger);
    this.api.interceptors.response.use(
      AxiosLogger.responseLogger,
      AxiosLogger.errorLogger,
    );
  }

  public async postFile(uri: string) {
    const form = new FormData();
    form.append('file', {
      uri,
      type: 'image/jpeg',
      name: uri,
    });
    return this.post<void>('files', form, {
      headers: {
        ...this.delegate.getHeaders(),
        'Content-Type': 'multipart/form-data',
        'data': form,
      },
    });
  }

  protected request<T>(config: AxiosRequestConfig): Promise<T> {
    return ApiBase.wrapApiCall(this.api.request(config));
  }

  protected get<T>(url: string, config?: AxiosRequestConfig): Promise<T> {
    return ApiBase.wrapApiCall(this.api.get(url, config));
  }

  protected delete(url: string, config?: AxiosRequestConfig): Promise<void> {
    return ApiBase.wrapApiCall(this.api.delete(url, config));
  }

  protected head(url: string, config?: AxiosRequestConfig): Promise<void> {
    return ApiBase.wrapApiCall(this.api.head(url, config));
  }

  protected post<T>(url: string, data?: any, config?: AxiosRequestConfig): Promise<T> {
    return ApiBase.wrapApiCall(this.api.post(url, data, config));
  }

  protected put<T>(url: string, data?: any, config?: AxiosRequestConfig): Promise<T> {
    return ApiBase.wrapApiCall(this.api.put(url, data, config));
  }

  protected patch<T>(url: string, data?: any, config?: AxiosRequestConfig): Promise<T> {
    return ApiBase.wrapApiCall(this.api.patch(url, data, config));
  }

  private static handleError(error: AxiosError) {
    const {response} = error;
    if (!response) {
      return new ApiError(error);
    }

    return new ApiHttpError(response.status, response.statusText, response.data, error);
  }

  private static handleResponse<T>(response: AxiosResponse<T>) {
    return response.data;
  }
}
