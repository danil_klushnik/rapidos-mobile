import ApiBase from './core/ApiBase';
import Session from 'auth/Session';
import RegisterRequest from '../entities/RegisterRequest';
import LoginRequest from '../entities/LoginRequest';
import RefreshRequest from 'api/entities/RefreshRequest';
import ForgotPasswordRequest from 'api/entities/ForgotPasswordRequest';

export default class RestApi extends ApiBase {
  public async register(request: RegisterRequest) {
    return this.post<Session>(`auth/register`, request);
  }

  public async uploadFile(uri: string) {
    return this.postFile(uri);
  }

  public async login(request: LoginRequest) {
    return this.post<Session>('auth/login', request);
  }

  public async refresh(refreshRequest: RefreshRequest) {
    return this.post<Session>('auth/refresh', refreshRequest);
  }

  public async forgotPassword(request: ForgotPasswordRequest) {
    return this.post<void>('auth/forgotPassword', request);
  }
}
