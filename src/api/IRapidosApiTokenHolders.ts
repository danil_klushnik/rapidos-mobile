export default interface IRapidosApiTokenHolders {
  getToken(): RapidosApiToken | undefined;
  setToken(token: RapidosApiToken | undefined): void;
}

export type RapidosApiToken = string;
