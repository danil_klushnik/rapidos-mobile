import {IRapidosApi} from 'api/IRapidosApi';
import RegisterRequest from './entities/RegisterRequest';
import LoginRequest from './entities/LoginRequest';
import RestApi from 'api/rest/RestApi';
import ApiConfiguration from './core/ApiConfiguration';
import ForgotPasswordRequest from 'api/entities/ForgotPasswordRequest';
import {AuthInfoKeeper} from 'auth';
import {RapidosApiTokenHolders} from 'api/index';
import ApiDelegate from 'api/core/ApiDelegate';
import {
  getHeaders,
} from 'api/RapidosApiUtils';

export default class RapidosApi implements IRapidosApi {
  private readonly restApi: RestApi;

  constructor(configuration: ApiConfiguration) {
    let baseUrl = `${configuration.url}:${configuration.port}`;
    if (configuration.globalPrefix) baseUrl += configuration.globalPrefix;

    const delegate: ApiDelegate = {
      getHeaders,
    };

    this.restApi = new RestApi(baseUrl, configuration.rest, delegate);
  }

  public async register(request: RegisterRequest) {
    return this.restApi.register(request);
  }

  public async login(request: LoginRequest) {
    return this.restApi.login(request);
  }

  public async refreshTokens() {
    const authInfo = await AuthInfoKeeper.getAuthInfo();
    if (!authInfo) {
      throw new Error('Not authorized');
    }
    const authResponse = await this.restApi.refresh({
      refreshToken: authInfo.refreshToken,
    });
    await AuthInfoKeeper.update(authResponse);
    RapidosApiTokenHolders.setToken(authResponse.jwt);
  }

  public async forgotPassword(request: ForgotPasswordRequest) {
    return this.restApi.forgotPassword(request);
  }
}
