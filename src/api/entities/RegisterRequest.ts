export default interface RegisterRequest {
  name: string;
  lastName: string;
  email: string;
  phoneNumber: string;
  password: string;
}
