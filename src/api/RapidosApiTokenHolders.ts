import IRapidosApiTokenHolders, {RapidosApiToken} from 'api/IRapidosApiTokenHolders';

export default class RapidosApiTokenHolders implements IRapidosApiTokenHolders {
  private token: RapidosApiToken | undefined = undefined;

  setToken(token: RapidosApiToken | undefined) {
    this.token = token;
  }

  getToken(): RapidosApiToken | undefined {
    return this.token;
  }
}
