import RegisterRequest from 'auth/RegisterRequest';
import ApiRegisterRequest from 'api/entities/RegisterRequest';
import LoginRequest from 'auth/LoginRequest';
import ApiLoginRequest from 'api/entities/LoginRequest';

export const mapRegisterRequestToApi = (
  registerRequest: RegisterRequest,
): ApiRegisterRequest => ({
  name: registerRequest.name,
  email: registerRequest.email,
  phoneNumber: registerRequest.phoneNumber,
  password: registerRequest.password,
  lastName: registerRequest.lastName,
});

export const mapLoginRequestToApi = (
  loginRequest: LoginRequest,
): ApiLoginRequest => ({
  email: loginRequest.email,
  password: loginRequest.password,
});
