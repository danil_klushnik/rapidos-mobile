import React, {useState} from 'react';
import {Provider} from 'react-redux';
import {I18nextProvider} from 'react-i18next';
import i18n from 'i18next';
import AppDebugHeader from 'app/debug/AppDebugHeader';
import {getBuildType} from '../services/config/ConfigUtils';
import BuildType from '../entities/BuildType';
import AppLoading from './AppLoading';
import AppInitializer, {AppTypeHolder} from './AppInitializer';
import {logError} from '../utils/Logger';
import Router from '../routes/Router';
import AppType from '../entities/AppType';
import {getStore} from "../state";

const App: React.FC = () => {
  const buildType = getBuildType();
  const isDebug = buildType !== BuildType.Develop;

  const [isReady, setIsReady] = useState<boolean>(false);

  if (!isReady) {
    return (
      <AppLoading
        startAsync={async () => {
          // if (appType !== getAppType())
          //   throw new Error('appType malformed');
          AppTypeHolder.appType = AppType.Client;
          await AppInitializer.initAsync();
        }}
        onFinish={() => setIsReady(true)}
        onError={logError}
      />
    );
  }

  return (
    <Provider store={getStore()}>
      <I18nextProvider i18n={i18n}>
        <Router />
      </I18nextProvider>
      {isDebug && (
        <AppDebugHeader buildType={buildType} appType={AppType.Client} />
      )}
    </Provider>
  );
};

export default App;
