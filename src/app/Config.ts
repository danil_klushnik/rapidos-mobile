import {Platform as RNPlatform} from 'react-native';
import {Platform} from 'entities/Platform';
import {getBuildType} from 'services/config/ConfigUtils';
import BuildType from 'entities/BuildType';
import AppType from 'entities/AppType';
import {AppTypeHolder} from "./AppInitializer";

const getAppType = (): AppType => {
  const {appType} = AppTypeHolder;
  if (!appType) throw new Error('appType is not defined');
  return appType;
};

const getPlatform = (): Platform => {
  switch (RNPlatform.OS) {
    case 'android':
      return Platform.Android;
    case 'ios':
      return Platform.iOS;
    default:
      throw new Error(`Unknown platform: ${RNPlatform.OS}`);
  }
};

export default {
  getAppType,
  getPlatform,
};

export const isClient = () => getAppType() === AppType.Client;
export const isCourier = () => getAppType() === AppType.Courier;


export const isDevelop = () => getBuildType() === BuildType.Develop;
export const isProduction = () => getBuildType() === BuildType.Production;
