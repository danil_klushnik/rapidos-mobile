enum AppType {
  Client = 'Client',
  Courier = 'Courier',
}

export default AppType;
