enum BuildType {
  Develop = 'Develop',
  Production = 'Production',
}

export default BuildType;
