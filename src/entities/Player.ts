export default interface Player {
  id: string;
  name: string;
  externalId: string;
  height: string;
  weight: string;
  bats: string;
  throws: string;
  graduatingClass: string;
  primaryPosition: string;
  highSchool: string;
  contactPhone: string;
  highSchoolContactPhone: string;
  statePositionRanking: string;
  stateOverallRanking: string;
  nationalPositionRanking: string;
  nationalOverallRanking: string;
  collegeCommitment: string;
}
