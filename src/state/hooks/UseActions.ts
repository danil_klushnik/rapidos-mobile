import {useDispatch} from 'react-redux';
import {useHistory} from 'react-router-native';
// import {actions as authActions} from '../ducks/auth';
// import UpdateUserRequest from '../ducks/session/models';
// import {actions as sessionActions} from '../ducks/session';
import {actions as routerActions} from '../ducks/router';
// import RegisterRequest from '../../auth/RegisterRequest';
// import LoginRequest from '@spryrocks/react-auth/LoginRequest';
// import ForgotPasswordRequest from '../../api/entities/ForgotPasswordRequest';
// import UpdatePreferences from '../../api/entities/UpdatePreferences';

export function useAuthActions() {
  // const dispatch = useDispatch();

  return {
    // registerUser: (registerRequest: RegisterRequest) => {
    //   dispatch(authActions.registerUser({request: registerRequest, history}));
    // },
    // login: (loginRequest: LoginRequest) =>
    //   dispatch(authActions.login({request: loginRequest, history})),
    // updateUserProfile: (updateRequest: UpdateUserRequest) => {
    //   dispatch(sessionActions.updateUserProfile(updateRequest));
    // },
    // logout: () => dispatch(authActions.logout({history})),
    // recoverPassword: (email: ForgotPasswordRequest) => {
    //   dispatch(authActions.recoverPassword({request: email, history}));
    // },
  };
}

export function useRouterActions() {
  const dispatch = useDispatch();
  const history = useHistory();

  return {
    goBack: () => {
      dispatch(routerActions.goBack({history}));
    },
    navigateToAppartements: () => {
      dispatch(routerActions.navigateToAppartements({history}));
    },
    navigateToBoutiques: () => {
      dispatch(routerActions.navigateToBoutiques({history}));
    },
    navigateToLivereus: () => {
      dispatch(routerActions.navigateToLivereus({history}));
    },
    navigateToRestaurants: () => {
      dispatch(routerActions.navigateToRestaurants({history}));
    },
    navigateToSupermarches: () => {
      dispatch(routerActions.navigateToSupermarches({history}));
    },
    navigateToTaxis: () => {
      dispatch(routerActions.navigateToTaxis({history}));
    },
    navigateToAuth: () => {
      dispatch(routerActions.navigateToAuth({history}));
    },
    navigateToMain: () => {
      dispatch(routerActions.navigateToMain({history}));
    },
  };
}
