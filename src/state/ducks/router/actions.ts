import {createAction} from 'redux-actions';
import types from './types';
import * as H from 'history';

export type NavigationPayload = {history: H.History};

export default {
  goBack: createAction<NavigationPayload>(types.GO_BACK),
  accountEntered: createAction<NavigationPayload>(types.ACCOUNT_ENTERED),
  navigateToAuth: createAction<NavigationPayload>(types.NAVIGATE_TO_AUTH),
  navigateToMain: createAction<NavigationPayload>(types.NAVIGATE_TO_MAIN),

  navigateToTaxis: createAction<NavigationPayload>(types.NAVIGATE_TO_TAXIS),
  navigateToAppartements: createAction<NavigationPayload>(types.NAVIGATE_TO_APPARTEMENTS),
  navigateToBoutiques: createAction<NavigationPayload>(types.NAVIGATE_TO_BOUTIQUES),
  navigateToLivereus: createAction<NavigationPayload>(types.NAVIGATE_TO_LIVREUR),
  navigateToRestaurants: createAction<NavigationPayload>(types.NAVIGATE_TO_RESTAURANTS),
  navigateToSupermarches: createAction<NavigationPayload>(types.NAVIGATE_TO_SUPERMARCHES),
};
