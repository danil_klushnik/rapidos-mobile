import {all, put, select, takeEvery} from 'redux-saga/effects';
import types from './types';
import {NavigationPayload} from './actions';
import {Action} from 'redux-actions';
import State from 'state/entities/State';
import {actions as sessionActions} from 'state/ducks/session';
import {LoadableContainer} from 'entities/LoadableContainer';

function goBack({payload}: Action<NavigationPayload>) {
  payload.history.goBack();
}

function* accountEntered({payload}: Action<NavigationPayload>) {
  const session: LoadableContainer<any> = yield select(
    (state: State) => state.session,
  );
  if (!session.isSuccess && !session.isLoading) {
    yield put(sessionActions.fetchSession(payload));
  }
}

function navigateToAuth({payload}: Action<NavigationPayload>) {
  payload.history.push('/auth');
}

function navigateToMain({payload}: Action<NavigationPayload>) {
  payload.history.push('/main');
}

function navigateToRestaurants({payload}: Action<NavigationPayload>) {
  payload.history.push('/main/restaurants');
}

function navigateToLivereus({payload}: Action<NavigationPayload>) {
  payload.history.push('/main/livereus');
}

function navigateToBoutiques({payload}: Action<NavigationPayload>) {
  payload.history.push('/main/boutiques');
}

function navigateToAppartements({payload}: Action<NavigationPayload>) {
  payload.history.push('/main/appartements');
}

function navigateToTaxis({payload}: Action<NavigationPayload>) {
  payload.history.push('/main/taxis');
}

function navigateToSupermarches({payload}: Action<NavigationPayload>) {
  payload.history.push('/main/supermarches');
}

export default function* () {
  yield all([
    takeEvery(types.GO_BACK, goBack),
    takeEvery(types.NAVIGATE_TO_AUTH, navigateToAuth),
    takeEvery(types.ACCOUNT_ENTERED, accountEntered),
    takeEvery(types.NAVIGATE_TO_MAIN, navigateToMain),

    takeEvery(types.NAVIGATE_TO_TAXIS, navigateToTaxis),
    takeEvery(types.NAVIGATE_TO_APPARTEMENTS, navigateToAppartements),
    takeEvery(types.NAVIGATE_TO_BOUTIQUES, navigateToBoutiques),
    takeEvery(types.NAVIGATE_TO_LIVREUR, navigateToLivereus),
    takeEvery(types.NAVIGATE_TO_RESTAURANTS, navigateToRestaurants),
    takeEvery(types.NAVIGATE_TO_SUPERMARCHES, navigateToSupermarches),
  ]);
}
