import {all, put, takeEvery} from 'redux-saga/effects';
// import {RapidosApi as Api} from 'api';
import {Action} from 'redux-actions';
// import {Account} from 'entities/Account';
import types from './types';
// import sessionActions from 'state/ducks/session/actions';
import {errorActions} from 'state/ducks/error';
import Session from 'auth/Session';


function* fetchSession() {
  try {
    // const account: Account = yield Api.myAccount();
    // yield put(sessionActions.fetchUserCompleted(account));
    // yield put(sessionActions.setSessionExists(true));
  } catch (e) {
    yield put(errorActions.handleError(e));
  }
}

function* fetchUserCompleted({error}: Action<Session>) {
  if (error) {
    yield put(errorActions.handleError(error));
    return;
  }
}

export default function*() {
  yield all([
    takeEvery(types.FETCH_SESSION, fetchSession),
    takeEvery(types.FETCH_USER_COMPLETED, fetchUserCompleted),
  ]);
}
