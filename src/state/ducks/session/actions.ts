import {createAction} from 'redux-actions';
import types from './types';
import {Account} from 'entities/Account';

export default {
  fetchSession: createAction(types.FETCH_SESSION),
  setSessionExists: createAction<boolean>(types.SET_SESSION_EXISTS),
  fetchUserAccount: createAction(types.FETCH_USER_ACCOUNT),
  fetchUserCompleted: createAction<Account>(types.FETCH_USER_COMPLETED),
};
