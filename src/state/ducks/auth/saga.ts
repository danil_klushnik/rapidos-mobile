import types from './types';
import {all, put, takeEvery} from 'redux-saga/effects';
import {Action} from 'redux-actions';
import sessionActions from '../session/actions';
import routerActions, {NavigationPayload} from '../router/actions';
import Session from 'auth/Session';
import {RapidosApi as Api} from 'api';
import {AuthInfoKeeper} from 'auth';
import {mapLoginRequestToApi, mapRegisterRequestToApi} from 'api/Mappers';
import actions, {
  AuthCompleted,
  Login,
  RecoverPassword,
  RegisterUser,
} from './actions';
import {errorActions} from '../error';
import {snackBarActions} from '../snackBar';

function* registerUser({payload}: Action<RegisterUser>) {
  const {history, request} = payload;
  try {
    const session: Session = yield Api.register(
      mapRegisterRequestToApi(request),
    );
    yield put(actions.authCompleted({history, session}));
  } catch (e) {
    yield put(errorActions.handleError(e));
  }
}

function* authCompleted({payload, error}: Action<AuthCompleted>) {
  const {history, session} = payload;

  if (error) {
    yield put(errorActions.handleError(session));
    return;
  }

  yield AuthInfoKeeper.authenticate(session);
  yield put(sessionActions.setSessionExists(true));
  yield put(routerActions.navigateToMain({history}));
  yield put(sessionActions.fetchSession());
}

function* logout({payload}: Action<NavigationPayload>) {
  yield AuthInfoKeeper.reset();
  yield put(routerActions.navigateToAuth(payload));
}

function* loginUser({payload}: Action<Login>) {
  try {
    const {history, request} = payload;
    const session: Session = yield Api.login(mapLoginRequestToApi(request));
    yield put(actions.authCompleted({session, history}));
  } catch (e) {
    yield put(errorActions.handleError(e));
  }
}

function* recoverPassword({payload}: Action<RecoverPassword>) {
  try {
    const {request, history} = payload;
    yield Api.forgotPassword(request);
    yield put(routerActions.goBack({history}));
  } catch (e) {
    yield put(errorActions.handleError(e));
  }
}

function* recoverPasswordCompleted({
  payload,
  error,
}: Action<NavigationPayload>) {
  if (error) {
    yield put(yield put(errorActions.handleError(payload)));
    return;
  }
  yield put(
    snackBarActions.showSnackbar({
      message: 'Password successfully changed!',
      type: 'success',
    }),
  );
  yield put(routerActions.goBack(payload));
}

export default function* () {
  yield all([
    takeEvery(types.LOGOUT, logout),
    takeEvery(types.REGISTER_USER, registerUser),
    takeEvery(types.AUTH_COMPLETED, authCompleted),
    takeEvery(types.LOGIN_USER, loginUser),
    takeEvery(types.RECOVER_PASSWORD, recoverPassword),
    takeEvery(types.RECOVER_PASSWORD_COMPLETED, recoverPasswordCompleted),
  ]);
}
