export default interface RegisterRequest {
  name: string;
  email: string;
  phoneNumber: string;
  password: string;
  lastName: string;
}
